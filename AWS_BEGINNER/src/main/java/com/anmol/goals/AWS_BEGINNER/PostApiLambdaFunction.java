package com.anmol.goals.AWS_BEGINNER;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

/**
 * Hello world!
 *
 */
public class PostApiLambdaFunction
		implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

	private Connection con;
	private PreparedStatement preparedStatement;

	public static final String objectKey = "PostApi/data";

	public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
		String requestString = input.getBody();
		JSONParser parser = new JSONParser();
		con = MyConnectionManager.getConnection();
		JSONArray jsonArray = null;
		try {
			if (requestString != null) {
				jsonArray = (JSONArray) parser.parse(requestString);
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return getResponse(input.getPath(), input.getBody(), "response", "Error in parsing JSON Array");
		}
		if (jsonArray != null && jsonArray.size() > 0) {
			for (int i = 0; i < jsonArray.size(); ++i) {
				JSONObject obj = (JSONObject) jsonArray.get(i);
				try {
					preparedStatement = con
							.prepareStatement("INSERT INTO employee (first_name,last_name) VALUES (?,?)");
				} catch (SQLException e) {
					e.printStackTrace();
					return getResponse(input.getPath(), input.getBody(), "response",
							"Error in connecting with Database");
				}
				try {
					preparedStatement.setString(1, (String) obj.get("firstName"));
					preparedStatement.setString(2, (String) obj.get("lastName"));
					preparedStatement.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
					return getResponse(input.getPath(), input.getBody(), "response",
							"Error in adding data in Database");
				}
			}
			return getResponse(input.getPath(), input.getBody(), "response", "Data Added Successfully");
		} else {
			return getResponse(input.getPath(), input.getBody(), "response", "No data provided to add");
		}
	}

	private String getContent(String path, String requestBody, String output) {
		StringBuilder returnValue = new StringBuilder();
		Calendar cal = Calendar.getInstance();
		returnValue.append("API: " + path);
		returnValue.append(" with request body =" + requestBody);
		returnValue.append("\n");
		returnValue.append("Date: " + cal.getTime() + "\n");
		returnValue.append(output);
		returnValue.append("\n\n");
		returnValue.append("-------------------------------------------------------------");
		return returnValue.toString();
	}

	private APIGatewayProxyResponseEvent getResponse(String path, String requestBody, String key, String response) {
		JSONObject obj = new JSONObject();
		obj.put(key, response);
		APIGatewayProxyResponseEvent output = new APIGatewayProxyResponseEvent();
		output.setBody(obj.toString());
		InputDataIntoS3.uploadObjectToS3(objectKey, getContent(path, requestBody, obj.toString()));
		return output;
	}

}
