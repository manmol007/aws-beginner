package com.anmol.goals.AWS_BEGINNER;

import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

/**
 * Hello world!
 *
 */
public class GetApiLambdaFunction implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

	private Connection con;
	private PreparedStatement preparedStatement;

	public static final String objectKey = "GetApi/data";

	public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {

		Map<String, String> queryParameter = input.getQueryStringParameters();
		Integer employeeId = null;
		JSONArray returnArray = new JSONArray();
		String query = "SELECT * FROM employee";
		if (queryParameter != null && queryParameter.get("employeeId") != null) {
			employeeId = Integer.parseInt(queryParameter.get("employeeId"));
			query = "SELECT * FROM employee where id=?";
		}
		con = MyConnectionManager.getConnection();
		try {
			preparedStatement = con.prepareStatement(query);
			if (employeeId != null) {
				preparedStatement.setInt(1, employeeId);
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				JSONObject obj = new JSONObject();
				obj.put("employeeId", resultSet.getLong(1));
				obj.put("firstName", resultSet.getString(2));
				obj.put("lastName", resultSet.getString(3));
				returnArray.add(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		InputDataIntoS3.uploadObjectToS3(objectKey, getContent(input.getPath(), employeeId, returnArray));
		return getResponse("response", returnArray);
	}

	private String getContent(String input, Integer employeeId, JSONArray output) {
		StringBuilder returnValue = new StringBuilder();
		Calendar cal = Calendar.getInstance();
		returnValue.append("API: " + input);
		if (employeeId != null) {
			returnValue.append(" with query parameter employeeId=" + employeeId);
		}
		returnValue.append("\n");
		returnValue.append("Date: " + cal.getTime() + "\n");
		returnValue.append("response:" + output.toJSONString());
		returnValue.append("\n\n");
		returnValue.append("-------------------------------------------------------------");
		return returnValue.toString();
	}

	private APIGatewayProxyResponseEvent getResponse(String key, JSONArray returnArray) {
		JSONObject obj = new JSONObject();
		obj.put(key, returnArray);
		APIGatewayProxyResponseEvent output = new APIGatewayProxyResponseEvent();
		output.setBody(obj.toString());
		return output;
	}
}
