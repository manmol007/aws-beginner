package com.anmol.goals.AWS_BEGINNER;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;

public class MyConnectionManager {

	private static final String URL = "jdbc:postgresql://database-1.c7mgls9hveqg.us-east-2.rds.amazonaws.com:5432/test";
	private static final String USERNAME = "postgres";
	private static final String PASSWORD = "postgres";
  

	public static Connection getConnection() {
		
		
		Connection con = null;
		try {
//			Class.forName(DRIVER);
			con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			System.out.println("DataBase Connection successful");

		} catch (SQLException e) {
			System.out.println("Errorcode:"+e.getErrorCode()+"Mesage:"+e.getMessage()+"Time:"+new Date()+"\n");
		} 
//		catch (ClassNotFoundException e1) {
//			System.out.println("Message:"+e1.getMessage()+"Time:"+new Date()+"\n");
//			
//		}
		return con;

	}

}
